from flask import Flask, request, jsonify
import cv2
import face_recognition
import json

app = Flask(__name__)

@app.route('/process', methods=['POST'])
def process():
	data = request.data.decode('utf-8')
	data_dict = json.loads(data)
	# print(data_dict['data'])
	public = '/home/jimmy/Documents/Projects/Working_on/Node-app/public'
	file = data_dict['file']

	path = f'{public}/images/{file}'
	destination = f'{public}/result/'

	# nparr = np.fromstring(request.data, np.uint8)
	# received_img = cv2.imdecode(nparr, cv2.IMREAD_COLOR)

	received_img = cv2.imread(path)

	# converting RGB image to grey scale
	grey_img = cv2.cvtColor(received_img, cv2.COLOR_RGB2GRAY)
	# locate face
	detected_faces = face_recognition.face_locations(grey_img)

	# reorder list
	for top, right, bottom, left in detected_faces:
		x1, x2, y1, y2 = left, right, top, bottom

	cv2.rectangle(received_img, (int(x1), int(y1)), (int(x2), int(y2)), (0, 255, 0), 2)
	cv2.imwrite(destination+file, received_img)

	coordinates = {
		'x1': x1,
		'x2': x2,
		'y1': y1,
		'y2': y2
	}

	return jsonify({ "path": file}), 200

app.run(host='localhost', port=5001)
