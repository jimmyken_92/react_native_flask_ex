import React, { Component } from 'react'
import { View, Text, Image, Button, StyleSheet } from 'react-native'
import ImagePicker from 'react-native-image-picker'

const createFormData = (photo, body) => {
    const data = new FormData()

    data.append("photo", {
        name: photo.fileName,
        type: photo.type,
        uri:
        Platform.OS === "android" ? photo.uri : photo.uri.replace("file://", "")
    })

    Object.keys(body).forEach(key => {
        data.append(key, body[key])
    })

    return data
}

export default class Client extends Component {
    constructor(props) {
        super(props)
        this.state = {
            photo: null,
            image: null
        }
    }

    handleChoosePhoto = () => {
        this.setState({ image: null })
        const options = {
            noData: true,
        }
        ImagePicker.launchImageLibrary(options, response => {
            if (response.uri) {
                this.setState({ photo: response })
            }
        })
    }

    handleFaceDetection = () => {
        alert("Uploading")
        fetch("http://192.168.0.100:3001/api/upload", {
            method: "POST",
            body: createFormData(this.state.photo, { action: 0 })
        })
        .then(response => response.json())
        .then(response => {
            this.setState({ image: 'http://192.168.0.100:3001/result/'+response.file.path }),
            alert("Upload success!")
            console.log(this.state.image)
            this.setState({ photo: null })
        })
        .catch(error => {
            console.log("upload error")
            alert("Upload failed!")
        })
    }
    handlePoseEstimation = () => {
        alert("Uploading")
        fetch("http://192.168.0.100:3001/api/upload", {
            method: "POST",
            body: createFormData(this.state.photo, { action: 1 })
        })
        .then(response => response.json())
        .then(response => {
            this.setState({ image: 'http://192.168.0.100:3001/result/'+response.file.path }),
            alert("Upload success!")
            console.log(this.state.image)
            this.setState({ photo: null })
        })
        .catch(error => {
            console.log("upload error")
            alert("Upload failed!")
        })
    }

    render() {
        const { photo } = this.state
        return (
            <View style={ styles.content }>
                {photo && (
                    <React.Fragment>
                        <Image
                         source={{ uri: photo.uri }}
                         style={{ width: 300, height: 550 }}
                        />
                        <View style={ styles.buttonContent }>
                            <Button color="red" title="Detect faces" onPress={ this.handleFaceDetection }/>
                            <Text></Text>
                            <Button color="red" title="Estimate pose" onPress={ this.handlePoseEstimation }/>
                        </View>
                    </React.Fragment>
                )}
                {this.state.image ?
                    (<Image source={{ uri: this.state.image }} style={ styles.image } />)
                :
                    (<Text></Text>)
                }
                <Button title="Choose Photo" onPress={ this.handleChoosePhoto } />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    content: {
        height: '95%',
        flexDirection: 'column',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    buttonContent: {
        width: 130
    },
    image: {
        marginTop: 50,
        width: 300,
        height: 500,
        resizeMode: 'stretch'
    }
})
