# React Native face detection microservices

## How to use
1. (Client) Install the React Native project on your mobile phone using `react-native run-android` or `react-native run-ios`
2. (Server) Run the Node.js server with `npm start`
3. (Server) Start the Flask server inside the `py` folder with the command `python3 server.py`
4. Using the app, choose an image and press `Send`
5. Look inside `Server/result`

