const Express = require('express')
const multer = require('multer')
const bodyParser = require('body-parser')

const request = require('request')

const app = Express()
app.use(bodyParser.json())

const Storage = multer.diskStorage({
    destination(req, file, callback) {
        callback(null, './public/images')
    },
    filename(req, file, callback) {
        callback(null, `${file.fieldname}_${Date.now()}_${file.originalname}`)
    },
})

const upload = multer({ storage: Storage })

app.use(Express.static('public'))

app.get('/', (req, res) => {
    res.status(200).send('You can post to /api/upload.')
})

app.post('/api/upload', upload.array('photo', 3), (req, res) => {
    var port = ''
    if(res.status(200)) {
        if(req.body.action == 0) {
            port = '5001'
        } else if(req.body.action == 1) {
            port = '5002'
        }
        console.log(req.files[0].filename)

        var url = `http://localhost:${port}/process`
        var destination = req.files[0].destination
        var file = req.files[0].filename

        const options = {
            url,
            json: true,
            body: {destination: destination, file: file}
        }
        const result = request.post(options, function (err, response, body) {
            if (err) {
                throw err
            }
            res.status(200).json({
                file: response.body,
            })
        })
    }
})

app.listen(3001, () => {
    console.log('App running on http://localhost:3001')
})
